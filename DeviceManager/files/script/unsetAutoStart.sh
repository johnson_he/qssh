#!/bin/sh

if [ -e /etc/init.d/pw_sh ]; then
  rm -rf /etc/init.d/pw_sh

  sed -i 's/^#\(\/.*-qws.*$\)/\1/g' /etc/init.d/rcS
  sed -i '/^\/etc\/init.d\/pw_sh$/d' /etc/init.d/rcS

  sync
  echo "unset auto start success."
else
  echo "Not found pw_sh"
fi
