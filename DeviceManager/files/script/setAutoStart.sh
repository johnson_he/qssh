#!/bin/sh

if [ -e /mnt/sdcard/sd/script/pw_sh ]; then
  cp /mnt/sdcard/sd/script/pw_sh /etc/init.d/
  chmod 777 /etc/init.d/pw_sh

  sed -i 's/\(^\/.*-qws.*$\)/#\1\n\/etc\/init.d\/pw_sh/g' /etc/init.d/rcS

  sync
  echo "set auto start success."
elif [ -e /mnt/usbdisk/sda1/script/pw_sh ]; then
  cp /mnt/usbdisk/sda1/script/pw_sh /etc/init.d/
  chmod 777 /etc/init.d/pw_sh

  sed -i 's/\(^\/.*-qws.*$\)/#\1\n\/etc\/init.d\/pw_sh/g' /etc/init.d/rcS

  sync
  echo "set auto start success."
else
  echo "Not found pw_sh"
fi
