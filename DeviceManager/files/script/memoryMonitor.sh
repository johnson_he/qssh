#!/bin/sh

memoryMonitorLog=/mnt/sdcard/sd/memoryMonitor.log

recordMemoryUsage()
{
  threadId=`ps | awk '/[app\/]BAMSv30/ {print $1}'`

  curTime=`date +"%Y_%m_%d %H:%M:%S"`
  echo "Time: "$curTime >> $memoryMonitorLog

  cat /proc/$threadId/status | awk '/Vm/ {if(match($1,"VmPeak")) {print $0} if(match($1,"VmSize")) {print 
$0}}' >> $memoryMonitorLog

  echo "" >> $memoryMonitorLog
}

while [ 1 > 0 ]
do
  recordMemoryUsage
  sleep 30
done

