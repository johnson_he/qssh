#include "devicemanager.h"
#include <qfileinfo.h>
#include "configs.h"

#define MOVE_TO_NEXT_CMD    do{ \
    this->m_currentCmdIndex++; \
    this->m_currentCmdState = DMS_DELAYING; \
    }while(0)

CDeviceManager::CDeviceManager(QObject *parent)
    :QObject(parent)
{
    this->m_sshConnection = NULL;
}

CDeviceManager::~CDeviceManager()
{
    this->Release();
}

void CDeviceManager::ProcessDevice(const QString &host,
                                    int port,
                                    int timeout,
                                    const QString &name,
                                    const QString &password,
                                    const QString &config)
{
    this->Release();

    ConfigParser parser;
    if(parser.parse(this->m_cmds, config))
    {
        emit this->finished("CDeviceManager::ProcessDevice: failed to parse config.");
        return;
    }

    this->m_sshParams.host = host;
    this->m_sshParams.port = port;
    this->m_sshParams.userName = name;
    this->m_sshParams.authenticationType = QSsh::SshConnectionParameters::AuthenticationByPassword;
    this->m_sshParams.password = password;
    this->m_sshParams.timeout = timeout;

    this->m_currentCmdIndex = 0;
    this->m_currentCmdState = DMS_DELAYING;
    this->m_encounterError = false;

    this->ConnectToDevice();
}

QString CDeviceManager::GetLastError()
{
    return this->m_lastError;
}

void CDeviceManager::Release()
{
    if(this->m_sshConnection)
    {
        this->m_sshConnection->deleteLater();
        this->m_sshConnection = NULL;
    }

    this->m_cmds.clear();
}

int CDeviceManager::ConnectToDevice()
{
    this->m_sshConnection = new SshConnection(this->m_sshParams, this);
    connect(this->m_sshConnection, SIGNAL(connected()), this, SLOT(OnConnect()));
    connect(this->m_sshConnection, SIGNAL(error(QSsh::SshError)), this, SLOT(OnConnectError(QSsh::SshError)));

    emit this->stdOutString("CDeviceManager::ConnectToDevice: connecting to device...");
    this->m_sshConnection->connectToHost();
    return 0;
}

void CDeviceManager::CreateSftpChannel()
{
    DM_Command cmd = m_cmds.at(m_currentCmdIndex);

    this->m_ftpChannel = this->m_sshConnection->createSftpChannel();
    connect(this->m_ftpChannel.data(), SIGNAL(initialized()), this, SLOT(OnSftpInitialized()));
    connect(this->m_ftpChannel.data(), SIGNAL(initializationFailed(QString)),
            this, SLOT(OnSftpInitializationFailed(QString)));
    connect(this->m_ftpChannel.data(), SIGNAL(finished(QSsh::SftpJobId,QString)),
            this, SLOT(OnSftpFinished(QSsh::SftpJobId,QString)));

    emit this->stdOutString(QString("CDeviceManager::CreateSftpChannel: %1 file from %2 to %3.")
                            .arg(cmd.oper == OPER_DOWNLOAD ? "Downloading":"Uploading")
                            .arg(cmd.src)
                            .arg(cmd.dst));
    this->m_ftpChannel->initialize();
}

void CDeviceManager::ExcuteOneCommand()
{
    DM_Command cmd = m_cmds.at(m_currentCmdIndex);

    this->m_encounterError = false;

    switch (cmd.oper)
    {
    case OPER_RECONNECT:
        this->reconnectToHost(cmd);
        break;
    case OPER_UPLOAD:
        this->uploadFile(cmd);
        break;
    case OPER_DOWNLOAD:
        this->downloadFile(cmd);
        break;
    case OPER_EXCUATE:
        this->executeRemoteCommand(cmd);
        break;
    default:
        emit this->finished("CDeviceManager::ExcuteOneCommand: unsupport operation.");
        break;
    }
}

bool CDeviceManager::checkExcuteResult()
{
    if((this->m_currentCmdIndex + 1 < this->m_cmds.count()) &&
       (this->m_cmds.at(this->m_currentCmdIndex + 1).oper == OPER_RECONNECT))
    {
        // if next command is RECONNECT, then ignore all errors
        return true;
    }

    // error happened
    if(this->m_encounterError)
    {
        return false;
    }

    // remote disconnect
    if((this->m_sshConnection) &&
       (this->m_sshConnection->state() != SshConnection::Connected))
    {
        return false;
    }

    // remote process timeout
    if((this->m_remoteProcess) &&
       (this->m_remoteProcess->isRunning()))
    {
        this->m_remoteProcess->close();
        return false;
    }

    return true;
}

void CDeviceManager::reconnectToHost(const DM_Command &cmd)
{
    if(this->m_sshConnection)
    {
        this->m_sshConnection->closeAllChannels();
        this->m_sshConnection->disconnectFromHost();
        this->m_sshConnection->deleteLater();
        this->m_sshConnection = NULL;
    }

    this->ConnectToDevice();
}

void CDeviceManager::uploadFile(const DM_Command &cmd)
{
    this->CreateSftpChannel();
}

void CDeviceManager::downloadFile(const DM_Command &cmd)
{
    this->CreateSftpChannel();
}

void CDeviceManager::executeRemoteCommand(const DM_Command &cmd)
{
//    this->m_remoteProcess = this->m_sshConnection->createRemoteProcess(cmd.src.toLatin1());
//    connect(this->m_remoteProcess.data(), SIGNAL(readyReadStandardOutput()), this, SLOT(OnReadyReadStdOutput()));
//    connect(this->m_remoteProcess.data(), SIGNAL(readyReadStandardError()), this, SLOT(OnReadyReadStdError()));
//    connect(this->m_remoteProcess.data(), SIGNAL(closed(int)), this, SLOT(OnRemoteProcessClose(int)));

//    emit this->stdOutString(QString("CDeviceManager::executeRemoteCommand: excuting cmd: %1.").arg(
//                                cmd.src));
//    this->m_remoteProcess->start();

    this->m_shell->write(cmd.src.toAscii() + "\r\n");

    emit this->stdOutString(QString("CDeviceManager::executeRemoteCommand: excuting cmd: %1.").arg(
                                cmd.src));
}

void CDeviceManager::SetErrorString(const QString &errorString)
{
    this->m_lastError = errorString;
    emit this->finished(this->m_lastError);
}

void CDeviceManager::OnScheduleTimerTimeout()
{
    DM_Command cmd;

    switch (m_currentCmdState)
    {
    case DMS_DELAYING:
        cmd = m_cmds.at(m_currentCmdIndex);
        this->m_currentCmdState = DMS_EXCUTING;
        this->m_scheduleTimer.start(cmd.delayBeforeMs);
        break;
    case DMS_EXCUTING:
        cmd = m_cmds.at(m_currentCmdIndex);
        this->m_encounterError = false;
        this->ExcuteOneCommand();

        this->m_currentCmdState = DMS_WAITING_EXECUTE;
        // scheduleTimer will be start after OnConnect slot
        if(cmd.oper != OPER_RECONNECT)
        {
            this->m_scheduleTimer.start(cmd.delayAfterMs);
        }
        break;
    case DMS_WAITING_EXECUTE:
        if(this->checkExcuteResult())
        {
            this->m_currentCmdIndex++;
            if(this->m_currentCmdIndex < m_cmds.count())
            {
                this->m_currentCmdState = DMS_DELAYING;
                this->m_scheduleTimer.start(1);
            }
            else
            {
                emit this->finished("CDeviceManager::OnScheduleTimerTimeout: all job done.");
                this->m_currentCmdState = DMS_END;
            }
        }
        else
        {
            emit this->finished("CDeviceManager::OnScheduleTimerTimeout: encount error while executing command.");
            this->m_currentCmdState = DMS_END;
        }
        break;
    default:    // finish
        if(this->m_sshConnection)
        {
            this->m_sshConnection->disconnectFromHost();
            this->m_sshConnection->deleteLater();
            this->m_sshConnection = NULL;
        }
        if(this->m_scheduleTimer.isActive())
        {
            this->m_scheduleTimer.stop();
        }
        break;
    }
}

void CDeviceManager::OnConnect()
{
    this->m_shell = this->m_sshConnection->createRemoteShell();
    connect(this->m_shell.data(), SIGNAL(readyReadStandardOutput()), this, SLOT(OnReadyReadStdOutput()));
    connect(this->m_shell.data(), SIGNAL(readyReadStandardError()), this, SLOT(OnReadyReadStdError()));
    connect(this->m_shell.data(), SIGNAL(closed(int)), this, SLOT(OnRemoteProcessClose(int)));
    this->m_shell->start();


    this->m_scheduleTimer.setSingleShot(true);
    connect(&m_scheduleTimer, SIGNAL(timeout()), this, SLOT(OnScheduleTimerTimeout()));

    this->m_scheduleTimer.start(1);
}

void CDeviceManager::OnConnectError(QSsh::SshError state)
{
    this->m_encounterError = true;
    this->SetErrorString(this->m_sshConnection->errorString());
}

void CDeviceManager::OnSftpInitialized()
{
    SftpJobId id;
    DM_Command cmd = this->m_cmds.at(m_currentCmdIndex);

    if(cmd.oper == OPER_UPLOAD)
    {
        QFileInfo fileInfo(cmd.src);
        if(fileInfo.isFile())
        {
            id = this->m_ftpChannel->uploadFile(cmd.src, cmd.dst,
                                                SftpOverwriteExisting);
        }
        else if(fileInfo.isDir())
        {
            id = this->m_ftpChannel->uploadDir(cmd.src, cmd.dst);
        }
        else
        {
            emit this->finished("CDeviceManager::OnSftpInitialized: unknow file type of : " + cmd.src);
        }
    }
    else
    {
        id = this->m_ftpChannel->downloadFile(cmd.src, cmd.dst,
                                                  SftpOverwriteExisting);
    }
    if(id == QSsh::SftpInvalidJob)
    {
        this->SetErrorString("CDeviceUpgrader::OnInitialized: could not start an upload job.");
    }
}

void CDeviceManager::OnSftpInitializationFailed(const QString &errorString)
{
    this->m_encounterError = true;
    this->SetErrorString(errorString);
}

void CDeviceManager::OnSftpFinished(QSsh::SftpJobId id, const QString &retString)
{
    if(! retString.isEmpty())
    {
        this->m_encounterError = true;
        this->SetErrorString(retString);
    }
    else
    {
        this->m_scheduleTimer.start(1);
    }

    emit this->stdOutString("CDeviceManager::OnFinished: file transmit finished.");
}

void CDeviceManager::OnReadyReadStdOutput()
{
    emit this->stdOutString(this->m_shell->readAllStandardOutput());
}

void CDeviceManager::OnReadyReadStdError()
{
    emit this->stdErrorString(this->m_shell->readAllStandardError());
}

void CDeviceManager::OnRemoteProcessClose(int ret)
{
    if(m_currentCmdIndex < m_cmds.count())
    {
        emit this->stdOutString(QString("CDeviceManager::OnRemoteProcessClose: %1 finished, ret: %2.")
                                .arg(m_cmds.at(m_currentCmdIndex).src)
                                .arg(ret == SshRemoteProcess::NormalExit ? "Normal exit":"failed to start or crash."));
    }
}
