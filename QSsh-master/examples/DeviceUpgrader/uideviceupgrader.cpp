#include "uideviceupgrader.h"
#include "ui_uideviceupgrader.h"
#include <qfiledialog.h>
#include <qdesktopservices.h>
#include <qapplication.h>
#include <qurl.h>
#include <qtextcodec.h>

UIDeviceUpgrader::UIDeviceUpgrader(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::UIDeviceUpgrader)
{
    ui->setupUi(this);

    this->m_deviceManager = NULL;

    connect(ui->pbbrowser, SIGNAL(clicked(bool)), this, SLOT(OnClickBrowser()));
    connect(ui->pbopen, SIGNAL(clicked(bool)), this, SLOT(OnClickOpen()));
    connect(ui->pbupgrade, SIGNAL(clicked(bool)), this, SLOT(OnClickExecute()));
}

UIDeviceUpgrader::~UIDeviceUpgrader()
{
    delete ui;
}

void UIDeviceUpgrader::OnClickBrowser()
{
    QString workDir = QApplication::applicationDirPath() + "/scripts";
    QString filePath = QFileDialog::getOpenFileName(this, tr("选择配置文件"), workDir, "Config file(*.txt)");

    if(! filePath.isEmpty())
    {
        ui->lepacketPath->setText(filePath);
    }
}

void UIDeviceUpgrader::OnClickOpen()
{
    QString filePath = ui->lepacketPath->text();

    QUrl url = QUrl::fromLocalFile(filePath);

    QDesktopServices::openUrl(url);
}

void UIDeviceUpgrader::OnClickExecute()
{
    if(this->m_deviceManager)
    {
        delete this->m_deviceManager;
    }

    QString path = ui->lepacketPath->text();
    QString host = ui->lehost->text();
    int port = ui->leport->text().toInt();
    int timeout = ui->letimeout->text().toInt() * 1000;
    QString name = ui->lename->text();
    QString password = ui->lepassword->text();

    ui->telog->clear();

    this->m_deviceManager = new CDeviceManager(this);
    connect(this->m_deviceManager, SIGNAL(stdOutString(QString)), this, SLOT(OnExecuteFinished(QString)));
    connect(this->m_deviceManager, SIGNAL(stdErrorString(QString)), this, SLOT(OnExecuteFinished(QString)));
    connect(this->m_deviceManager, SIGNAL(finished(QString)), this, SLOT(OnExecuteFinished(QString)));
    this->m_deviceManager->ProcessDevice(host, port, timeout, name, password, path);
}

void UIDeviceUpgrader::OnExecuteFinished(const QString &retString)
{
    ui->telog->append(retString);
}
