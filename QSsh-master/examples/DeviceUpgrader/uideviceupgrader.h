#ifndef UIDEVICEUPGRADER_H
#define UIDEVICEUPGRADER_H

#include <QWidget>
#include <qstring.h>
#include "devicemanager.h"

namespace Ui {
class UIDeviceUpgrader;
}

class UIDeviceUpgrader : public QWidget
{
    Q_OBJECT

public:
    explicit UIDeviceUpgrader(QWidget *parent = 0);
    ~UIDeviceUpgrader();

private slots:
    void OnClickBrowser();
    void OnClickOpen();
    void OnClickExecute();
    void OnExecuteFinished(const QString &retString);
private:
    Ui::UIDeviceUpgrader *ui;

    CDeviceManager *m_deviceManager;
};

#endif // UIDEVICEUPGRADER_H
