#ifndef CONFIGS_H
#define CONFIGS_H

#define DU_DEST_FILE_PATH    "/mnt/userdata2/pw/upgrade/package.zip"
#define DU_UPGRADE_SCRIPT_PATH  "/mnt/userdata2/pw/upgrade/upgrade.sh"
#define DU_UPGRAGE_SUCCESS_TAG  "PW_UPGRAGE_SUCCESSED"

#include <qdebug.h>
#define dulog   qDebug

#endif // CONFIGS_H
