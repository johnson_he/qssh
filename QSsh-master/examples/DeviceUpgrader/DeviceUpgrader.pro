#-------------------------------------------------
#
# Project created by QtCreator 2017-01-10T14:56:53
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DeviceManager
TEMPLATE = app

INCLUDEPATH = $$PWD/../../src/libs/ssh/

SOURCES += main.cpp\
        uideviceupgrader.cpp \
    devicemanager.cpp \
    configparser.cpp

HEADERS  += uideviceupgrader.h \
    configs.h \
    devicemanager.h \
    configparser.h

FORMS    += uideviceupgrader.ui

include(../../qssh.pri) ## Required for IDE_LIBRARY_PATH and qtLibraryName
LIBS += -L$$IDE_LIBRARY_PATH -l$$qtLibraryName(Botan) -l$$qtLibraryName(QSsh)
