#include "configparser.h"
#include <qfile.h>
#include <qtextstream.h>
#include <qstringlist.h>
#include <qtextcodec.h>
#include <qapplication.h>

#define COMMENT_STR     "#"
#define ACTION_RECONNECT    "RECONNECT"
#define ACTION_UPLOAD     "UPLOAD"
#define ACTION_DOWNLOAD    "DOWNLOAD"
#define ACTION_EXECUTE    "EXECUTE"
#define SPLIT_STR       ","

ConfigParser::ConfigParser()
{

}

int ConfigParser::parse(QList<DM_Command> &cmds, const QString &path)
{
    QString lineStr;
    QFile file(path);
    int lineNum = 1;
    int ret = 0;

    if(! file.open(QIODevice::ReadOnly))
    {
        return -1;
    }

    QTextStream configStream(&file);

    while(! configStream.atEnd())
    {
        lineStr = configStream.readLine().trimmed();

        if(lineStr.isEmpty())
        {
            // skip empty lines
            lineNum++;
            continue;
        }
        else if(lineStr.startsWith(COMMENT_STR))
        {
            lineNum++;
            continue;
        }
        else if(lineStr.startsWith(ACTION_RECONNECT))
        {
            ret = this->parseReconnect(cmds, lineStr);
            if(ret < 0)
            {
                break;
            }

            lineNum++;
            continue;
        }
        else if(lineStr.startsWith(ACTION_UPLOAD))
        {
            ret = this->parseUpload(cmds, lineStr);
            if(ret < 0)
            {
                break;
            }

            lineNum++;
            continue;
        }
        else if(lineStr.startsWith(ACTION_DOWNLOAD))
        {
            ret = this->parseDownload(cmds, lineStr);
            if(ret < 0)
            {
                break;
            }

            lineNum++;
            continue;
        }
        else if(lineStr.startsWith(ACTION_EXECUTE))
        {
            ret = this->parseExecute(cmds, lineStr);
            if(ret < 0)
            {
                break;
            }

            lineNum++;
            continue;
        }
        else
        {
            ret = -2;
            break;
        }
    }

    file.close();

    return ret;
}

int ConfigParser::parseReconnect(QList<DM_Command> &cmds, const QString &line)
{
    DM_Command cmd;
    QStringList strList = line.split(SPLIT_STR, QString::SkipEmptyParts);

    if(strList.count() < 3)
    {
        return -1;
    }

    cmd.oper = OPER_RECONNECT;
    cmd.delayBeforeMs = strList.at(1).trimmed().toUInt();
    cmd.delayAfterMs = strList.at(2).trimmed().toUInt();

    cmds.append(cmd);

    return 0;
}

int ConfigParser::parseUpload(QList<DM_Command> &cmds, const QString &line)
{
    DM_Command cmd;
    QStringList strList = line.split(SPLIT_STR, QString::SkipEmptyParts);
    if(strList.count() < 5)
    {
        return -1;
    }

    cmd.oper = OPER_UPLOAD;
    cmd.src = strList.at(1).trimmed();
    cmd.dst = strList.at(2).trimmed();
    cmd.delayBeforeMs = strList.at(3).trimmed().toUInt();
    cmd.delayAfterMs = strList.at(4).trimmed().toUInt();

    formatLocalFilePath(cmd.src);

    cmds.append(cmd);

    return 0;
}

int ConfigParser::parseDownload(QList<DM_Command> &cmds, const QString &line)
{
    DM_Command cmd;
    QStringList strList = line.split(SPLIT_STR, QString::SkipEmptyParts);
    if(strList.count() < 5)
    {
        return -1;
    }

    cmd.oper = OPER_DOWNLOAD;
    cmd.src = strList.at(1).trimmed();
    cmd.dst = strList.at(2).trimmed();
    cmd.delayBeforeMs = strList.at(3).trimmed().toUInt();
    cmd.delayAfterMs = strList.at(4).trimmed().toUInt();

    formatLocalFilePath(cmd.dst);

    cmds.append(cmd);

    return 0;
}

int ConfigParser::parseExecute(QList<DM_Command> &cmds, const QString &line)
{
    DM_Command cmd;
    QStringList strList = line.split(SPLIT_STR, QString::SkipEmptyParts);
    if(strList.count() < 4)
    {
        return -1;
    }

    cmd.oper = OPER_EXCUATE;
    cmd.src = strList.at(1).trimmed();
    cmd.delayBeforeMs = strList.at(2).trimmed().toUInt();
    cmd.delayAfterMs = strList.at(3).trimmed().toUInt();

    cmds.append(cmd);

    return 0;
}

void ConfigParser::formatLocalFilePath(QString &path)
{
    if(path.startsWith("."))
    {
        path.remove(0,1);
        path = QApplication::applicationDirPath() + path;
    }
}
