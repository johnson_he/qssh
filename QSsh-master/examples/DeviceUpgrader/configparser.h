#ifndef CONFIGPARSER_H
#define CONFIGPARSER_H

#include <qstring.h>
#include <qlist.h>

enum Operation
{
    OPER_RECONNECT,
    OPER_UPLOAD,
    OPER_DOWNLOAD,
    OPER_EXCUATE,
};

struct DM_Command
{
    Operation oper;
    QString src;
    QString dst;
    uint delayBeforeMs;
    uint delayAfterMs;
};

class ConfigParser
{
public:
    ConfigParser();

    int parse(QList<DM_Command> &cmds, const QString &path);

private:
    int parseReconnect(QList<DM_Command> &cmds, const QString &line);
    int parseUpload(QList<DM_Command> &cmds, const QString &line);
    int parseDownload(QList<DM_Command> &cmds, const QString &line);
    int parseExecute(QList<DM_Command> &cmds, const QString &line);

    void formatLocalFilePath(QString &path);
};

#endif // CONFIGPARSER_H
