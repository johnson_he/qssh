#ifndef DEVICEUPGRADER_H
#define DEVICEUPGRADER_H

#include <qstring.h>
#include <QTimer>
#include "sshconnection.h"
#include "sftpchannel.h"
#include "sshremoteprocess.h"
#include "configparser.h"

using namespace QSsh;

enum DeviceManagerState
{
    DMS_DELAYING,
    DMS_EXCUTING,
    DMS_WAITING_EXECUTE,
    DMS_END
};

class CDeviceManager: public QObject
{
    Q_OBJECT
public:
    CDeviceManager(QObject *parent = 0);
    ~CDeviceManager();

    void ProcessDevice(const QString &host,
                       int port,
                       int timeout,
                       const QString &name,
                       const QString &password,
                       const QString &config);
    QString GetLastError();
signals:
    void stdOutString(const QString &out);
    void stdErrorString(const QString &error);
    void finished(const QString &retString);

private slots:
    void OnScheduleTimerTimeout();

    void OnConnect();
    void OnConnectError(QSsh::SshError state);

    void OnSftpInitialized();
    void OnSftpInitializationFailed(const QString &errorString);
    void OnSftpFinished(QSsh::SftpJobId id, const QString &retString);

    void OnReadyReadStdOutput();
    void OnReadyReadStdError();
    void OnRemoteProcessClose(int ret);

private:
    void Release();
    int ConnectToDevice();
    void ExcuteOneCommand();
    bool checkExcuteResult();

    void reconnectToHost(const DM_Command &cmd);
    void uploadFile(const DM_Command &cmd);
    void downloadFile(const DM_Command &cmd);
    void executeRemoteCommand(const DM_Command &cmd);

    void CreateSftpChannel();

    void SetErrorString(const QString &errorString);

private:
    QList<DM_Command> m_cmds;
    int m_currentCmdIndex;
    QTimer m_scheduleTimer;
    DeviceManagerState m_currentCmdState;
    bool m_encounterError;

    QString m_lastError;

    SshConnectionParameters m_sshParams;
    SshConnection *m_sshConnection;
    QSharedPointer<SftpChannel> m_ftpChannel;

    QSharedPointer<SshRemoteProcess> m_remoteProcess;
    QSharedPointer<SshRemoteProcess> m_shell;
};

#endif // DEVICEUPGRADER_H
