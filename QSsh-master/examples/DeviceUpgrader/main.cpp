#include "uideviceupgrader.h"
#include <QApplication>
#include <qtextcodec.h>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));

    UIDeviceUpgrader w;
    w.show();

    return a.exec();
}
